﻿using System;
using System.Data.SqlClient;
using System.Data;
//using Microsoft.SqlServer.Management.Smo;
//using Microsoft.SqlServer.Management.Common;
//using System.IO;

namespace LoadDelaGetSFTP
{
    class CodeSQL : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public void RunStoredProc(string con, string sqlProc)
        {
            using (var conn = new SqlConnection(con))
            using (var command = new SqlCommand(sqlProc, conn)
            {
                CommandType = CommandType.StoredProcedure
            }
            )
            {
                try
                {
                    conn.Open();
                    command.ExecuteNonQuery();
                }
                //catch (SqlServerManagementException ex)
                //{
                    //string msg = String.Format("---SqlServerManagementException: {0} in>RunStoredProc", ex.Message);
                    //using (CodeCommon clsCode = new CodeCommon())
                    //{
                    //    clsCode.RecordToLog(LogFyleName, msg);
                    //}
                //}
                catch (SqlException ex)
                {
                    //string msg = String.Format("---SqlException: {0} in>RunStoredProc", ex.Message);
                    //using (CodeCommon clsCode = new CodeCommon())
                    //{
                    //    clsCode.RecordToLog(LogFyleName, msg);
                    //}
                }
                catch (Exception ex)
                {
                    //string msg = String.Format(" ---Exception: {0} in>RunStoredProc", ex.Message);
                    //using (CodeCommon clsCode = new CodeCommon())
                    //{
                    //    clsCode.RecordToLog(LogFyleName, msg);
                    //}
                }
            }
        }

    }
}
