﻿using System;
using System.Windows.Forms;

namespace LoadDelaGetSFTP
{
    public partial class FrmNone : Form
    {
        public FrmNone()
        {
            InitializeComponent();
        }

        private void FrmNone_Load(object sender, EventArgs e)
        {
            using (CodeSQL clsSql = new CodeSQL())
            {
                clsSql.RunStoredProc(Properties.Settings.Default.aconSql, "Matrix_Payments_b50_Export_sp");
            }
            Application.Exit();
        }
    }
}
